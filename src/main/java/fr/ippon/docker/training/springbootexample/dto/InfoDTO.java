package fr.ippon.docker.training.springbootexample.dto;

import java.time.LocalDateTime;

/**
 * InfoDTO
 */
public class InfoDTO {

    private String hostName;

    private LocalDateTime currentDateTime;

	/**
	 * @return the currentDateTime
	 */
	public LocalDateTime getCurrentDateTime() {
		return currentDateTime;
	}

	/**
	 * @param currentDateTime the currentDateTime to set
	 */
	public void setCurrentDateTime(LocalDateTime currentDateTime) {
		this.currentDateTime = currentDateTime;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
}